<?php
/**
 * Created by PhpStorm.
 * User: Tsvetelin Mayel
 * Date: 20.7.2018 г.
 * Time: 05:54 ч.
 */

namespace App\Http\Controllers;

use Exception;

class NumbersController extends Controller
{
    public function index($number){
        $error = null;
        $classification = null;

        try{
            $classification = self::getClassification((int)$number);
        }
        catch (\Exception $e){
            $error = $e->getMessage();
        }

        return response()->json([
            'classification' => $classification,
            'error' => $error
        ]);
    }

    public static function getClassification($integer)
    {
        // Note you have to use version of PHP 64 bits for numbers larger than 2^31
        // There is no need to use third party library such as Litipk\BigNumbers\Decimal
        // because PHP7 supports 64bit integers
        // My current vesrion is x86 and I can't test it with large number

        if (!is_int($integer)) {
            throw new Exception($integer.' is not integer');
        }

        if ($integer <= 0) {
            throw new Exception($integer.' has to be positive number');
        }

        $sqrt = sqrt($integer);
        // Why I'm using sqrt?
        // This is the shortest way to find how manu iterations from 1 I have to perform
        // every divisor before sqrt($integer) has corresponding divisor from "the other side" - the right side :)
        // so it is not nesessery to continue after this point - I already found all divisors grater than sqrt :-)

        // $aliquotSum will store the sum of the divisors
        // we can start with 1 because 1 is always is the first divisor
        $aliquotSum = 1;

        for ($i = 2; $i <= $sqrt; $i++) {

            if ($integer % $i == 0) {
                // here we know that $i is a divisor from the left side of the sqrt
                // now I have to find the corresponding divisor from the rightSide
                $rightSideDivisor = $integer / $i;

                //adding $i to the aliquotSum
                $aliquotSum += $i;

                if ($i != $rightSideDivisor) {
                    // if $i and $rightSide are different we will need
                    // both of them to be added to $aliquotSum
                    // also we don't have to worry about if $i or $rightSideDivisor is equal to $integer
                    // it will never happen because we are starting from $i=2 (not from $i=1)
                    $aliquotSum += $rightSideDivisor;
                }

            }

            if ($aliquotSum > $integer) {
                // we don't need to continue because we know that it's abundant
                return 'abundant';
            }
        }

        if ($aliquotSum < $integer) {
            return 'deficient';
        }

        // if we are here it's a perfect number!
        return 'perfect';
    }

}