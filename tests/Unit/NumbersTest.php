<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


class NumbersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNegativeError()
    {
        $response = $this->call('GET', 'api/get-number-type/-277');
        $data = json_decode($response->content());
        $this->assertEquals(true, $data->error != null);
    }

    public function testSmallPerfect()
    {
        $response = $this->call('GET', 'api/get-number-type/28');
        $data = json_decode($response->content());
        $this->assertEquals(true, $data->classification == 'perfect');
    }

    public function testBigPerfect()
    {
        $response = $this->call('GET', 'api/get-number-type/33550336');
        $data = json_decode($response->content());
        $this->assertEquals(true, $data->classification == 'perfect');
    }

    public function testBigAbundant()
    {
        $response = $this->call('GET', 'api/get-number-type/33550335');
        $data = json_decode($response->content());
        $this->assertEquals(true, $data->classification == 'abundant');
    }
}
